package ch.hslu.stud.pawi.biqa.minion.blure;

import ch.hslu.stud.pawi.biqa.minion.AlgorithmApplication;

public class Main {

    public static void main(String[] args) throws Exception {

        AlgorithmApplication app = new AlgorithmApplication();
        app.setAbstractAlgorithmFactory(new ConcreteBlureAlgorithmFactory());
        app.run(args);
    }
}

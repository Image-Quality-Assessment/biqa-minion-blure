package ch.hslu.stud.pawi.biqa.minion.blure;

import ch.hslu.stud.pawi.biqa.minion.AbstractAlgorithmFactory;
import ch.hslu.stud.pawi.biqa.minion.ImageQualityAssessmentAlgorithmus;

public class ConcreteBlureAlgorithmFactory implements AbstractAlgorithmFactory {
    @Override
    public ImageQualityAssessmentAlgorithmus createAlgorithm() {
        return new BlureCheck();
    }
}

package ch.hslu.stud.pawi.biqa.minion.blure;

import ch.hslu.stud.pawi.biqa.minion.ImageQuality;
import ch.hslu.stud.pawi.biqa.minion.ImageQualityAssessmentAlgorithmus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

public class BlureCheck implements ImageQualityAssessmentAlgorithmus {
    private static final Logger LOG = LoggerFactory.getLogger(BlureCheck.class);

    private static final Pattern PATTERN = Pattern.compile("score in main file is given by\\:([\\+|\\-|0-9\\.]+).*");

    @Override
    public ImageQuality check(File file) throws Exception {
        String executable = System.getProperty("user.dir") + "/" + "blure_detection";
        LOG.info("running: " + executable);
        ProcessBuilder builder = new ProcessBuilder(executable, file.getAbsolutePath());

        builder.directory(new File(System.getProperty("user.dir")));
        Process process = builder.start();
        process.waitFor();

        String output = getStdOut(process);
        if (process.exitValue() != 0) {
            throw new Exception("process did not exit with status 0");
        }

        float value = Float.valueOf(output);

        return new ImageQuality(value);
    }

    private String getStdOut(Process process) throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder output = new StringBuilder();
        String line;
        while ((line = input.readLine()) != null) {
            output.append(line);
        }
        input.close();
        return output.toString();
    }

    @Override
    public String getAlgorithmName() {
        return "blure detection";
    }
}
